//
// chat_server.cpp
// ~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
// 

// speed up crc message check by looking backwards in the loop then printing

#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <utility>
#include <boost/asio.hpp>
#include "chat_message.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <boost/bind.hpp>
//#include <boost/thread.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string.hpp>
#include <ctime>
#include <chrono>


using namespace boost::posix_time;
using namespace boost::gregorian;


std::string timeStamp(){

		std::string tStamp;
		time_t ltime;
		struct tm *Tm;
	 
		ltime=time(NULL);
		Tm=localtime(&ltime);
	 
		tStamp = std::to_string(Tm->tm_hour) +":"+ std::to_string(Tm->tm_min) +":"+ std::to_string(Tm->tm_sec);
		return tStamp;
	}

//CLASS PROTOTYPE
class chat_server;

using boost::asio::ip::tcp;

struct userData{			// Master Table of to send to clients of Users status/details
	int UUID;				// User's ID #, assigned by server
	std::string userName;	// Users chosen name
	int usersCurrRoom;		// The room the user is currently in
};

struct roomData{			// Master Table of Rooms to send to clients of Room status/details
	int roomNum;			// Room Number
	std::string roomName;	// Room Name
	std::string pass;		// Rooms Password
	int inviteOnly;			// Room invite only, yes or now, 1 = yes, 0 = no
	std::string hostName;	// The current host
};

roomData roomTable[10];
boost::mutex myLock;

std::vector <userData> userVector;
std::list<chat_server> servers;



//----------------------------------------------------------------------

typedef std::deque<chat_message> chat_message_queue;

//----------------------------------------------------------------------

class chat_participant
{
public:
	virtual ~chat_participant() {}
	virtual void deliver(const chat_message& msg) = 0;
	int UUID;												// Users Unique ID
	int roomNum;												// Users Current Room #
	std::string userName;									// Users Chosen Name
   
private:
  
};

typedef std::shared_ptr<chat_participant> chat_participant_ptr;

//----------------------------------------------------------------------

class chat_room
{
public:

	int UUIDcount = 0;

	void join(chat_participant_ptr participant)
	{
		participants_.insert(participant);
		for (auto msg: recent_msgs_)
		{
			participant->deliver(msg);							// send old messages to new users
		}
		participant->UUID = UUIDcount; 							// assign UUID to participant
		participant->roomNum = 0; 								// assign UUID to participant
		participant->userName = "/NULL";
		
		//printf("SOCKET: %s", participant->)
		
		//chat_message msg = notify_Client(std::to_string(participant->UUID), "REQUUID");		// create message for user with their UUID
		//participant->deliver(msg);															// send user their UUID
		//printMsg(msg);																		// print send message in console

		UUIDcount++;											// increment UUID by 1 each time someone new connects
	}
	
	// TESTING PRINT MSG =========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void printMsg(chat_message& msg){
		std::string theMessage = msg.body();
		printf("=-=-= Server Sending Message: %s \n\n" ,theMessage.c_str());
	}

	
	// MESSAGE TO VECTOR ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	std::vector<std::string> msgToVec(chat_message& msg){						// return vector of seperated strings
	  
		std::string theMessage = msg.body();
		std::vector<std::string> messageVec;									// vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		return messageVec;														// return vector of strings
	}
	
	// GET TIMESTAMP ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	std::string timeStamp(){

		ptime t = second_clock::local_time();
		std::string currentTime = to_iso_string(t);
		currentTime[15] = '.';				//Remove "," and change to "." to avoid error at server
		return currentTime;
		
	}
	
	// TRANSMIT MESSAGES TO CLIENT ===============================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////	
	
	void serverNotify_sendMsg(chat_message& msg){ 							// send message to the correct client
	// INCOMING: \MS,userName,lastCRC
	// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/SENDTEXT>, 3: <UserName>, 4: <lastCRC>
		
		
		std::vector <std::string> messageVec = msgToVec(msg);				// string split to vector
		int theirRoom = findUsersRoom(messageVec[3]);						// users current room
		
		//printf("SERVER SEND MSG: their room: %d, their last CRC: %d -----------------------\n",theirRoom, messageVec[2]);
		
		for (auto participant: participants_)								// Find the client to send to
		{
				if(participant->userName == messageVec[3]){
					
					msgPrintQueue( theirRoom, participant->userName, messageVec[4]);	//their room #, their username, their last CRC
					break;
				}
		}
	} 
	
	// TRANSMIT ROOM TABLE TO CLIENT ============================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////	
		
	void serverNotify_sendRoomTable(std::string theMessage){ 			// update RoomTable for Clients
		// Function will send a coded message to be sent to clients so they can update their table
		// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQCHATROOMS>, 3:<UserName>

		// RECIEVE CLIENTS INCOMING MESSAGE
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});	
		
		// CREATE MESSAGE TO BE SENT
		std::string roomTableString = ""; //string sent to client to update room table

		
		// CREATE STRING TO BE SENT
		for(int i = 0; i < 10; i++){
			roomTableString = roomTableString   + std::to_string(roomTable[i].roomNum) 	+ ","
												+ roomTable[i].roomName 				+ ","
												+ roomTable[i].pass						+ ","
												+ std::to_string(roomTable[i].inviteOnly)	+ ","
												+ roomTable[i].hostName					+ ",";
		}
		
		chat_message msg = notify_Client(roomTableString, "REQCHATROOMS");		// Create MessageArgs: room table string, Code
	
		// SEND MESSAGE TO REQUESTING CLIENT
		for (auto participant: participants_)								// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){				// Assign Username to first available UUID (a UUID with NULL currently associated with it
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
			
	}
	
	// TRANSMIT USER TABLE TO CLIENT ===========================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
  
	void serverNotify_sendUserTable(std::string theMessage){ 						// Function will send a coded message to clients so they can update their user tables
		// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQUSERS>, 3:<UserName>
		
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});	
		
		std::string userTableString = ""; 
		
		for(participant : participants_){
			userTableString = userTableString   + std::to_string(participant->UUID)		+ ","
												+ participant->userName 				+ ","
												+ std::to_string(participant->roomNum)	+ ",";
		}
		
		userTableString = userTableString + "/END";
		
		chat_message msg = notify_Client(userTableString, "REQUSERS");		// Create MessageArgs: users table string, Code
		
		// SEND MESSAGE TO REQUESTING CLIENT
		for (auto participant: participants_)					// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){	// Assign Username to first available UUID (a UUID with NULL currently associated with it
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
	}
	
	// TRANSMIT USER KICK ========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////	
		
	void serverNotify_Kickuser(std::string theMessage){ 			// Inform client of their current room
		// Function will send a coded message to be sent to clients so they can know what room they are currently in
		// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQCHATROOMS>, 3:<UserName>, 4:<Users Curr Room>

		// RECIEVE CLIENTS INCOMING MESSAGE
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});	
	
		std::string userKickString = messageVec[3] + "," + messageVec[4]; 
		chat_message msg = notify_Client("cya","KICKUSER");				// Create MessageArgs: nothing, Code
		
		//MOVE KICKED GUY TO LOBBY
		std::string changeRmMessage = messageVec[3] + "," + "0";						// create custom coding for room change:  user,roomNum(0 lobby)
		chat_message msg2 = notify_Client(changeRmMessage, "CHANGECHATROOM");			// create message to send to invitee. Args: Room#toinviteTo, "INVITE"
		deliver(msg2);
		
		// SEND MESSAGE TO KICKED USER
		for (auto participant: participants_)								// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){				// Assign Username to first available UUID (a UUID with NULL currently associated with it
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
			
		
	} 
	
	// TRANSMIT USERS CURR ROOM ==================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////	
		
	void serverNotify_sendChatroom(std::string theMessage){ 			// Inform client of their current room
		// Function will send a coded message to be sent to clients so they can know what room they are currently in
		// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQCHATROOMS>, 3:<UserName>

		// RECIEVE CLIENTS INCOMING MESSAGE
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});	
		
		chat_message msg = notify_Client(std::to_string(findUsersRoom(messageVec[3])), "REQUESTCHATROOM");		// Create MessageArgs: room#ofUser, Code
	
		// SEND MESSAGE TO REQUESTING CLIENT
		for (auto participant: participants_)					// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){	// Assign Username to first available UUID (a UUID with NULL currently associated with it
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
	}
	
	// TRANSMIT USERS UUID ==================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////	
		
	void serverNotify_sendUUID(std::string theMessage){ 			// Inform client of their current room
		// Function will send a coded message to be sent to clients so they can know what room they are currently in
		// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQCHATROOMS>, 3:<UserName>

		// RECIEVE CLIENTS INCOMING MESSAGE
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});	
		
		
	
		// SEND MESSAGE TO REQUESTING CLIENT
		for (auto participant: participants_)					// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){	// Assign Username to first available UUID (a UUID with NULL currently associated with it
					
					chat_message msg = notify_Client(std::to_string(participant->UUID), "REQUUID");		// Create MessageArgs: room#ofUser, Code
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
	}
	
	// USER LEAVES ROOM ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void leave(chat_participant_ptr participant)
	{
		participants_.erase(participant);
		
		std::cout << "leave" << std::endl;
		for(auto x : participants_){
			  printf("UUID: %d, NAME: %s ROOM: %d",x->UUID,x->userName.c_str(),x->roomNum);
			  printf("\n");
		}
	}
	
	// FIND USERS ROOM ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	int findUsersRoom(std::string usersname){		// pass in the vector of split up message strings
	int usersRoom = 0;	
	//printf("\nFIND USERS ROOM: username: %s\n",usersname.c_str());
		
			for (auto participant: participants_){
					if (participant->userName == usersname){		// user sends you their username, check the user table to find their room
						usersRoom = participant->roomNum;
					}
			}

		return usersRoom;
	}
	
	// MESSAGE PUSHBACK ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void msgPushback(chat_message& msg){								// add the message to that rooms message que
	// Incoming message from Client: //0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND/REQUSERS>, 3:<UserName>
	
		std::vector<std::string> messageVec = msgToVec(msg);			// vector of the separted strings from the message
		int usersRoom = findUsersRoom(messageVec[3]);					// the users room
			
		if(usersRoom == 1){
			room1_msgs_.push_back(msg);
		}
		else if(usersRoom == 2){
			room2_msgs_.push_back(msg);
		}
		else if(usersRoom == 3){
			room3_msgs_.push_back(msg);
		}
		else if(usersRoom == 4){
			room4_msgs_.push_back(msg);
		}
		else if(usersRoom == 5){
			room5_msgs_.push_back(msg);
		}
		else if(usersRoom == 6){
			room6_msgs_.push_back(msg);
		}
		else if(usersRoom == 7){
			room7_msgs_.push_back(msg);
		}
		else if(usersRoom == 8){
			room8_msgs_.push_back(msg);
		}
		else if(usersRoom == 9){
			room9_msgs_.push_back(msg);
		}
		else if(usersRoom == 10){
			room10_msgs_.push_back(msg);
		}
	  
	}
	
	// CLEAR CHAT QUEUE ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	void clearChatQueue(int roomNumber){								// add the message to that rooms message que
			
		if(roomNumber == 1){
			room1_msgs_.clear();
		}
		else if(roomNumber == 2){
			room2_msgs_.clear();
		}
		else if(roomNumber == 3){
			room3_msgs_.clear();
		}
		else if(roomNumber == 4){
			room4_msgs_.clear();
		}
		else if(roomNumber == 5){
			room5_msgs_.clear();
		}
		else if(roomNumber == 6){
			room6_msgs_.clear();
		}
		else if(roomNumber == 7){
			room7_msgs_.clear();
		}
		else if(roomNumber == 8){
			room8_msgs_.clear();
		}
		else if(roomNumber == 9){
			room9_msgs_.clear();
		}
		else if(roomNumber == 10){
			room10_msgs_.clear();
		}
	}
	
	// PRINT CHAT QUEUE ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	void msgPrintQueue(int roomNum, std::string username, std::string prevCRC){	// add the message to that rooms message que
	
		//printf("msgPrintQueue: RoomNum: %d, Username: %s, prevCRC: %s XXXXXXXXXXXXXXXXXXXXXX\n",roomNum,username.c_str(),prevCRC.c_str());
		
		chat_participant_ptr theParticipant;							// the person the server is going to send the message que to

		for (auto participant: participants_)							// Find the client to send message que to
			{
				if (participant->userName == username){				
					theParticipant = participant;
					break;
				}
			}
		
		int print = 0;														// When print becomes 1, print every message in que after that point
		if(prevCRC == "nullCRC"){											// non prev crc so print everything
			print = 1;
		}
		else{
			print = 0;
		}
		
		if(roomNum == 1){
			for (auto msg: room1_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 2){
			for (auto msg: room2_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 3){
			for (auto msg: room3_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 4){
			for (auto msg: room4_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 5){
			for (auto msg: room5_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 6){
			for (auto msg: room6_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 7){
			for (auto msg: room7_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 8){
			for (auto msg: room8_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 9){
			for (auto msg: room9_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
		else if(roomNum == 10){
			for (auto msg: room10_msgs_)
			{
				if(print == 1){
					theParticipant->deliver(msg);	// send old messages to new users
				}
				else if(msg.getCrc32FromMessage() == prevCRC){
					print = 1;
				}
			}
		}
	  
	}
	
	// DELIVER, RECIEVE CLIENT REQUESTS  =========================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////

  void deliver( chat_message& msg)
  {
	  // CHECK CRC ------------------------------------------------------------------------------------------------------
	  /*printf("Pre-deliveryCRC32: %d, checkCRC32: %d",msg.getCrc32FromMessage(),msg.checkCrc32());
	  
	  if(msg.checkCrc32() == msg.getCrc32FromMessage()){
		  printf("   CRC pass\n");
	  }
	  else{printf("   CRC fail\n");}
	   */

    std::string theMessage = msg.body();							//  the message text
	std::cout << "Servers Recieved Current Message: " << msg.body() << std::endl;	// CHECK THE MESSAGE AND DETERMINE WHAT TO DO WITH IT
	
	std::vector<std::string> messageVec = msgToVec(msg);			//vector of the separted strings from the message
	//printf("Deliver Vec:0:%s --- 1:%s \n", messageVec[0].c_str(),messageVec[1].c_str());
	std::string MAJORCOMMAND = messageVec[2];
		
		// CHANGE NAME =====================================================
		if(MAJORCOMMAND == "NICK"){												//*** The client is telling the server his nickname to be displayed
			//myLock.lock();
			serverNotify_ChangeName(theMessage);
			//myLock.unlock();
		}
		// CHANGE ROOM =====================================================
		else if(MAJORCOMMAND == "CHANGECHATROOM"){								//*** The client is requesting that it be changed to the given chatroom.
			myLock.lock();
			serverNotify_ChangeRoom(theMessage);
			myLock.unlock();
		}
		// CREATE ROOM =====================================================
		else if(MAJORCOMMAND == "NAMECHATROOM"){								//*** The client is requesting that a chatroom is created with the given name
			//myLock.lock();
			serverNotify_CreateRoom(theMessage);
			//myLock.unlock();
		}
		// RESET ROOM  ======================================================
		else if(MAJORCOMMAND == "RESETROOM"){									//*** 
			//myLock.lock();
			serverNotify_ResetRoom(theMessage);
			//myLock.unlock();
		}
		// INVITE USER ======================================================
		else if(MAJORCOMMAND == "INVITE"){										//*** 
			//myLock.lock();
			serverNotify_Invite(theMessage);
			//myLock.unlock();
		}
		// KICK USER ======================================================
		else if(MAJORCOMMAND == "KICKUSER"){									//*** 
			//myLock.lock();
			serverNotify_Kickuser(theMessage);
			//myLock.unlock();
		}
		// REQUEST ROOM TABLE ===============================================
		else if(MAJORCOMMAND == "REQCHATROOMS"){								//*** The client is requesting all of the chatrooms that are available
			//myLock.lock();
			serverNotify_sendRoomTable(theMessage);
			//myLock.unlock();
		}
		// REQUEST CURR ROOM ================================================
		else if(MAJORCOMMAND == "REQCHATROOM"){									//*** The client is requesting the name of the chatroom it is associated with
			//myLock.lock();
			serverNotify_sendChatroom(theMessage);
			//myLock.unlock();
		}
		// REQUEST USER TABLE ===============================================
		else if(MAJORCOMMAND == "REQUSERS"){									//*** The client is requesting a list of users and nicks.  This will be the users in the selected chatroom
			myLock.lock();
			serverNotify_sendUserTable(theMessage);
			myLock.unlock();
		}
		// REQUEST MESSAGES =================================================
		else if(MAJORCOMMAND == "REQTEXT"){										//*** The client is requesting all text that has been sent since the last time it was requested.
			myLock.lock();
			serverNotify_sendMsg(msg);
			myLock.unlock();
		}
		// SEND MESSAGE =====================================================
		else if(MAJORCOMMAND == "SENDTEXT"){									//*** The client is sending text to be sent to everyone in the chatroom.
			//myLock.lock();
			msgPushback(msg);													// add the message to that rooms message que
			//myLock.unlock();
		}
		// REQUEST UUID =====================================================
		else if(MAJORCOMMAND == "REQUUID"){									//*** The client is sending text to be sent to everyone in the chatroom.
			//myLock.lock();
			serverNotify_sendUUID(theMessage);
			//myLock.unlock();
		}
		
	/*
	while (recent_msgs_.size() > max_recent_msgs)						// delete items while queue is too large
	{
      recent_msgs_.pop_front();											// pop_front Removes (deletes) the first element in the deque container, effectively reducing its size by one.
	}

    */
	
}

	// USER CHANGES ROOM ==========================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void serverNotify_ChangeRoom(std::string theMessage){	// User Changing Room
	//update room table, incoming message format: \CR,UserName, newRoom
	//0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND>, 3:<UserName>, 4:<newRoom>
	
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		
		//printf("messageVec: 0:%s 1:%s \n", messageVec[0].c_str(),messageVec[1].c_str());
	
		int UsersRoomNum;															// The room # user is moving to
		std:: string usersName;
		
			for (auto participant: participants_)									// Assign Username to UUID
			{
				if (participant->userName == messageVec[3]){						// Change a users room #
					participant->roomNum = std::atoi(messageVec[4].c_str());
					UsersRoomNum = participant->roomNum;
					usersName = participant->userName;
					break;
				}
			}
			
			msgPrintQueue(UsersRoomNum, usersName, "nullCRC"); 						// Deliver Recent Messages upon user changing room
	}
	
	// RESET ROOM ================================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void serverNotify_ResetRoom(std::string theMessage){	// Reset Room name, password, kick all users in that room
	//update room table, incoming message format: \RR,Room#
	//0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND>, 3:<Room #>
	
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		printf("messageVec: 0:%s 1:%s \n", messageVec[0].c_str(),messageVec[1].c_str());
		
			// Reset the table back to original data
			int tableToReset = stoi(messageVec[3]);
		
			roomTable[tableToReset].hostName = "none";
			roomTable[tableToReset].inviteOnly = 0;
			roomTable[tableToReset].pass = "none";
			roomTable[tableToReset].roomName = "Room" + std::to_string(roomTable[tableToReset].roomNum);
			
			for (auto participant: participants_)						// Move users in the room back to lobby
			{
				if (participant->roomNum == tableToReset){				
					participant->roomNum = 0;
				}
			}
			clearChatQueue(tableToReset);								// Clear the Chat Queue
	} 
	
	// CREATE ROOM ===============================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void serverNotify_CreateRoom(std::string theMessage){	// Reset Room name, password, kick all users in that room
	//update room table, incoming message format: \CT,RoomName,hostName,Password,InviteOnly(1 or 0)
	//0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND>, 3:<RoomName>, 4:<hostName>, 5:<Password>, 6:<InviteOnly(1 or 0)>
	
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		//printf("messageVec: 0:%s 1:%s \n", messageVec[0].c_str(),messageVec[1].c_str());
		

		int i;
		for (i = 0; i < 10; i++)						// Find the first available room
			{
				if (roomTable[i].hostName == "none"){				
					break;
				}
			}
			
			printf("\n\n I: %d\n\n",i);
			
		// INSERT SOME KIND OF LOGIC FOR IF NO ROOMS ARE AVAILABLE

			// CREATE ROOM WITH USERS PROVIDED DETAILS
			roomTable[i].roomName = messageVec[3];
			roomTable[i].hostName = messageVec[4];
			roomTable[i].pass = messageVec[5];
			roomTable[i].inviteOnly = stoi(messageVec[6]);
			
			
			//MOVE HOST INTO THE ROOM
			std::string changeRmMessage = messageVec[4] + "," + std::to_string(roomTable[i].roomNum);	// create custom coding for room change, hostName.roomNum
			chat_message msg = notify_Client(changeRmMessage, "CHANGECHATROOM");						// create message to send to invitee. Args: Room#toinviteTo, "INVITE"
			deliver(msg);

	}
	
	// CREATE MESSAGE FOR CLIENT =================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	
	chat_message notify_Client(std::string myText, std::string MAJORCOMMAND)	// use this function to inform server special action to be taken
	{
		std::string notifyClientText = "";
		
		if (myText == "null"){
			notifyClientText = "," + timeStamp() + "," + MAJORCOMMAND + '\0';	// the message to send to server
		}
		else{
			notifyClientText = "," + timeStamp() + "," + MAJORCOMMAND + "," + myText + '\0';	// the message to send to server
		}
		chat_message msg;
		std::string theCRC = msg.createCrc32(notifyClientText);
		notifyClientText = theCRC + notifyClientText;
		
		msg.body_length(notifyClientText.length() + 1);			// the size of the message
		std::memset (msg.body(), 0, msg.body_length());			// ensure it is null terminated
		std::memcpy (msg.body(), ( const char *) notifyClientText.c_str(), msg.body_length()-1); // memcpy(dest,src,size)
		msg.encode_header();
		
		return msg;
	}
	
	// INVITE USER ===============================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////
	void serverNotify_Invite(std::string theMessage){		// User Inviting Other
		//invite, incoming message format: \IN,UserBeingInvitedName,RoomNumbertoInviteTo
		//0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND>, 3:<UserBeingInvitedName>, 4:<RoomNumbertoInviteTo>
		
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		
		//printf("messageVec: 0:%s 1:%s \n", messageVec[0].c_str(),messageVec[1].c_str(),messageVec[2].c_str());
		//std::string inviteCode = "INVITE," + messageVec[4];
		
		//Create coded message to send to invitee
		chat_message msg = notify_Client(messageVec[4], "INVITE");	// create message to send to invitee. Args: Room#toinviteTo, "INVITE"
		
			for (auto participant: participants_)					
			{
				if (participant->userName == messageVec[3]){		// Send message to invite user
					participant->deliver(msg);
					printMsg(msg);
					break;
				}
			}
	}
	
	// NEW USER ==================================================================================================================================================================
	//========================================================================================================================////////////////////////////////////////////////////

	void serverNotify_ChangeName(std::string theMessage){	// User Requesting Name Change
	//0: <CHECKSUM>, 1: <TIME>, 2: <MAJOR COMMAND>, 3:<UsersChosenName>

		
		std::vector<std::string> messageVec;	//vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		
		printf("CHANGE NAME: 0:%s 1:%s 2:%s 3:%s\n", messageVec[0].c_str(),messageVec[1].c_str(),messageVec[2].c_str(),messageVec[3].c_str());
		
			for (auto participant: participants_)					// Assign Username to UUID
			{
				if (participant->userName == "/NULL"){				// Assign Username to first available UUID (a UUID with NULL currently associated with it
					participant->userName = messageVec[3];
					break;
				}
			}
	}

private:
  std::set<chat_participant_ptr> participants_;
  enum { max_recent_msgs = 100 };
  chat_message_queue recent_msgs_;
  
  chat_message_queue room1_msgs_;		// 10 arrays(queues) for 10 rooms of chat log history
  chat_message_queue room2_msgs_;
  chat_message_queue room3_msgs_;
  chat_message_queue room4_msgs_;
  chat_message_queue room5_msgs_;
  chat_message_queue room6_msgs_;
  chat_message_queue room7_msgs_;
  chat_message_queue room8_msgs_;
  chat_message_queue room9_msgs_;
  chat_message_queue room10_msgs_;
  
};

// CHAT SESSION CLASS ========================================================================================================================================================
//========================================================================================================================////////////////////////////////////////////////////

class chat_session				// inherits from chat_participant(the abstract base class)
  : public chat_participant,
    public std::enable_shared_from_this<chat_session>
{
public:
	
  chat_session(tcp::socket socket, chat_room& room)
    : socket_(std::move(socket)),
      room_(room)
  {
  }

	
  void start()
  {
	
    room_.join(shared_from_this());
    do_read_header();
  }

  void deliver(const chat_message& msg)
  {
    bool write_in_progress = !write_msgs_.empty();
    write_msgs_.push_back(msg);
    if (!write_in_progress)
    {
      do_write();
    }
  }
  
  

private:
  void do_read_header()
  {
    auto self(shared_from_this());
    boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.data(), chat_message::header_length),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec && read_msg_.decode_header())
          {
            do_read_body();
          }
          else
          {
            room_.leave(shared_from_this());
          }
        });
  }

  void do_read_body()
  {
    auto self(shared_from_this());
    boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            room_.deliver(read_msg_);
            do_read_header();
          }
          else
          {
            room_.leave(shared_from_this());
          }
        });
  }

  void do_write()
  {
    auto self(shared_from_this());
    boost::asio::async_write(socket_,
        boost::asio::buffer(write_msgs_.front().data(),
          write_msgs_.front().length()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            write_msgs_.pop_front();
            if (!write_msgs_.empty())
            {
              do_write();
            }
          }
          else
          {
            room_.leave(shared_from_this());
          }
        });
  }

  
  tcp::socket socket_;
  chat_room& room_;
  chat_message read_msg_;
  chat_message_queue write_msgs_;
};

//----------------------------------------------------------------------

class chat_server
{
public:
  chat_server(boost::asio::io_service& io_service,
      const tcp::endpoint& endpoint)
    : acceptor_(io_service, endpoint),
      socket_(io_service)
  {
    do_accept();
  }
chat_room room_;

private:
  void do_accept()
  {
    acceptor_.async_accept(socket_,
        [this](boost::system::error_code ec)
        {
          if (!ec)
          {
            std::make_shared<chat_session>(std::move(socket_), room_)->start();
          }

          do_accept();
        });
  }

  tcp::acceptor acceptor_;
  tcp::socket socket_;
  
};


//----------------------------------------------------------------------

int main(int argc, char* argv[])
{
  try
  {
    if (argc < 2)
    {
      std::cerr << "Usage: chat_server <port> [<port> ...]\n";
      return 1;
    }

    boost::asio::io_service io_service;

	
	for (int i =0; i < 10; i++){						// Create the Master Table of Rooms
		roomData aRoom;									// A room struct
		aRoom.roomNum = i+1;							// room number
		aRoom.roomName = "Room" + std::to_string(i+1);	// room name
		aRoom.pass = "none";							// password
		aRoom.inviteOnly = 0;							// invite only?
		aRoom.hostName = "none";						// host name
		roomTable[i]=aRoom;								// add room to array of room
	}
    
    for (int i = 1; i < argc; ++i)
    {
      tcp::endpoint endpoint(tcp::v4(), std::atoi(argv[i]));
      servers.emplace_back(io_service, endpoint);
    }
	
	

    io_service.run();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception!: " << e.what() << "\n";
  }

  return 0;
}
