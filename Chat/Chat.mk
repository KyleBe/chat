##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Chat
ConfigurationName      :=Debug
WorkspacePath          := "/home/x/Code/Algo"
ProjectPath            := "/home/x/Code/Soft/chatproj/Chat"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=x
Date                   :=26/10/17
CodeLitePath           :="/home/x/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Chat.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -lfltk -lboost_system -lpthread
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)-lfltk -lboost_system -lpthread 
ArLibs                 :=  "-lfltk -lboost_system -lpthread" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -std=c++11 -Wall -O0 $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/chat_client.cpp$(ObjectSuffix) $(IntermediateDirectory)/chat_server.cpp$(ObjectSuffix) $(IntermediateDirectory)/client_gui.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/chat_client.cpp$(ObjectSuffix): chat_client.cpp $(IntermediateDirectory)/chat_client.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/x/Code/Soft/chatproj/Chat/chat_client.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/chat_client.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/chat_client.cpp$(DependSuffix): chat_client.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/chat_client.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/chat_client.cpp$(DependSuffix) -MM "chat_client.cpp"

$(IntermediateDirectory)/chat_client.cpp$(PreprocessSuffix): chat_client.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/chat_client.cpp$(PreprocessSuffix) "chat_client.cpp"

$(IntermediateDirectory)/chat_server.cpp$(ObjectSuffix): chat_server.cpp $(IntermediateDirectory)/chat_server.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/x/Code/Soft/chatproj/Chat/chat_server.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/chat_server.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/chat_server.cpp$(DependSuffix): chat_server.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/chat_server.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/chat_server.cpp$(DependSuffix) -MM "chat_server.cpp"

$(IntermediateDirectory)/chat_server.cpp$(PreprocessSuffix): chat_server.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/chat_server.cpp$(PreprocessSuffix) "chat_server.cpp"

$(IntermediateDirectory)/client_gui.cpp$(ObjectSuffix): client_gui.cpp $(IntermediateDirectory)/client_gui.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/x/Code/Soft/chatproj/Chat/client_gui.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/client_gui.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/client_gui.cpp$(DependSuffix): client_gui.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/client_gui.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/client_gui.cpp$(DependSuffix) -MM "client_gui.cpp"

$(IntermediateDirectory)/client_gui.cpp$(PreprocessSuffix): client_gui.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/client_gui.cpp$(PreprocessSuffix) "client_gui.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


