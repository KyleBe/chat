//
// chat_client.cpp
// ~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <deque>
#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include "chat_message.hpp"


using boost::asio::ip::tcp;

typedef std::deque<chat_message> chat_message_queue;

class chat_client
{
public:
  chat_client(    boost::asio::io_service& io_service,
      tcp::resolver::iterator endpoint_iterator,      void (*data_recv) (std::string S)    )
    : io_service_(io_service),     socket_(io_service),   data_recv_ ( data_recv )	// initialization list
  {
    do_connect(endpoint_iterator);
  }

	
	std::string getLastCRC(){	// get the last CRC the client recieved
	  return lastCRC;
	} 
  
	void setLastCRC(std::string theLastCRC){	//set the last CRC the client recieved
	  lastCRC = theLastCRC;
	} 
	
  void write( chat_message& msg)	// send the message over TCP
  {
	
	//msg.createCrc32();
	
	
    io_service_.post(
        [this, msg]()
        {
          bool write_in_progress = !write_msgs_.empty();
          write_msgs_.push_back(msg);
          if (!write_in_progress)
          {
            do_write();
          }
        });
		
		
  }

  void close()	// leave server
  {
    io_service_.post([this]() { socket_.close(); });
  }

private:
  void do_connect(tcp::resolver::iterator endpoint_iterator)	// connect to server
  {
    boost::asio::async_connect(socket_, endpoint_iterator,
        [this](boost::system::error_code ec, tcp::resolver::iterator)
        {
          if (!ec)
          {
            do_read_header();
          }
        });
		
  }

  void do_read_header()	//Read message header, determines body length
  {
	  
    boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.data(), chat_message::header_length),
        [this](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec && read_msg_.decode_header())
          {
            do_read_body();
          }
          else
          {
            socket_.close();
          }
        });
  }

  void do_read_body()	//Reads message body
  {
    boost::asio::async_read(socket_,
        boost::asio::buffer(read_msg_.body(), read_msg_.body_length()),
        [this](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
			std::string message = read_msg_.body();
			std::vector<std::string> messageVec;									// vector of the separted strings from the message
			boost::split(messageVec, message, [](char c){return c == ',';});
		
			if(messageVec[2] == "SENDTEXT" ){	// if the message isnt a coded message
				lastCRC = read_msg_.getCrc32FromMessage();
			}
			
			data_recv_ ( read_msg_.body() );
            do_read_header();
          }
          else
          {
            socket_.close();
          }
        });
  }
  
  

  void do_write()	// Transmit message
  {
    boost::asio::async_write(socket_,
        boost::asio::buffer(write_msgs_.front().data(),
          write_msgs_.front().length()),
        [this](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            write_msgs_.pop_front();
            if (!write_msgs_.empty())
            {
              do_write();
            }
          }
          else
          {
            socket_.close();
          }
        });
		
  }

private:
  boost::asio::io_service& io_service_;
  tcp::socket socket_;
  void (*data_recv_) (std::string S);

  chat_message read_msg_;
  chat_message_queue write_msgs_; 
  std::string lastCRC = "nullCRC";
};
#ifdef XXX
int mainxx(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: chat_client <host> <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;

    tcp::resolver resolver(io_service);
    auto endpoint_iterator = resolver.resolve({ argv[1], argv[2] });
    chat_client c(io_service, endpoint_iterator, ",");	// !!!!!!! CONNECT , last value is the users name

    std::thread t([&io_service](){ io_service.run(); });

    char line[chat_message::max_body_length + 1];	// the users chat line that gets sent to server
	
    while (std::cin.getline(line, chat_message::max_body_length + 1))	// repeat asking user for text
    {
      chat_message msg;
      msg.body_length(std::strlen(line));
      std::memcpy(msg.body(), line, msg.body_length());
      msg.encode_header();
      c.write(msg);
	  //printf("BOO!\n");
    }

    c.close();
    t.join();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
#endif
