// This example will demonstrate the integration of
// the boost asio chatserver with a simple FLTK program
//
//
//
#include <assert.h> 

#include "io.hpp"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Radio_Button.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Select_Browser.H>
#include <FL/Fl_Text_Display.H>
#include <cstring>
#include <string> 
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/date_formatting.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/gregorian/greg_month.hpp>
#include <mutex>          // std::mutex
#include <thread>         // std::thread
#include <Fl/Fl_Box.H>

using namespace boost::posix_time;
using namespace boost::gregorian;


// PROTOTYPES
void displayLogin();
void displayLobby();
static void cb_clear ();
static void notify_Server(std::string myText, std::string MAJORCOMMAND);	//declare after called in cb_recv()
std::string timeStamp();

//FLTK WIDGETS

Fl_Window win   (800, 500, "UberChat");
Fl_Input input1 (200, 340, 420, 20, "Enter text:");
Fl_Input userLogin (100, 20, 100, 20, "Username: ");
Fl_Input inviteUser (620, 340, 170, 20, "Invite: ");
Fl_Input kickUser (620, 380, 170, 20, "Kick: ");
Fl_Input rmName (100,340,100,20, "ROOM NAME:");
Fl_Input rmPassword (100,365,100,20, "PASSWORD:");
Fl_Check_Button privateCheck (120,390,100,20, "PRIVATE ROOM:");
Fl_Input cnfrmPass (500, 365,100, 20, "PASSWORD:");
Fl_Button rmChangeAccept (200, 365, 100, 20, "ACCEPT");
Fl_Button rmChangeDecline (305, 365, 100, 20, "DECLINE");
Fl_Button testMe (5, 475, 50, 20, "TEST");
Fl_Button cancelRm (115, 415, 70, 20, "CANCEL");
Fl_Button cnfrmRm  (10, 415, 40, 20, "CONFIRM");
Fl_Button clearBut (130, 325, 50, 20,"Clear");
Fl_Button createRm (10, 325, 115, 20,"CREATE ROOM");
Fl_Text_Buffer *buff    = new Fl_Text_Buffer ();		
Fl_Text_Display *disp = new Fl_Text_Display (200,70,420,250,"Chat");		
Fl_Browser *usrl = new Fl_Browser (620,70,170,250,"UUID User Name     Rm ");
Fl_Select_Browser *rmChoice = new Fl_Select_Browser (10,70,190,250,"Rm RmName    Pvt   Host  ");

// boost asio instances
chat_client *c = NULL;
std::thread *t = NULL;
std::string usersChosenName = "NULL";

//test boost instances
chat_client *testc = NULL;
std::thread *testt = NULL;

//std::mutex mtx;           // mutex for critical section
std::string usersCurrentRoom;
int userHost;
int maxUsr;
int newRoom;
int testsFailed;
int testsPassed;
std::string lastCRC;
std::string roomInvite;
std::string roomArray[10][5];
std::string userArray[10][3];
std::string myUUID;


//Test Variables
bool timeStampOn = true; 

static void cb_recv ( std::string S )
{
  // Note, this is an async callback from the perspective
  // of Fltk..
  //
  // high chance of a lock needed here if certain fltk calls
  // are made.  (like show() .... )
  //mtx.lock();
  Fl::lock();
  std::string T = S + '\n' + '\0';
  
	int i; //int
	int j;
	int k;
  
  if (buff) 
  {
		/*
	  if(T[0] != '/'){					// UNCOMMENT THIS LINE TO SEE THE INCOMING SERVER CODES
		buff->append ( T.c_str () );
	  }
	   */
	   
	std::string chatMsg; 
	std::vector<std::string>splitMsg;
	boost::split(splitMsg, T,[](char c){return c == ',';});
	int msgSize = splitMsg.size();
	std::string MAJORCOMMAND = splitMsg[2];
	
	if(MAJORCOMMAND=="SENDTEXT"){							//Displays messages for rooms

		i = 3;
		while (i < msgSize){
			if (i == 3) chatMsg = splitMsg[i] + ": ";			//prints NICK of user message was sent by
			else if (i == 4) chatMsg = chatMsg + splitMsg[i];
			else chatMsg = chatMsg + ", " + splitMsg[i];
			i++;
		}

		buff->append (chatMsg.c_str () ); 

	}	


	if(MAJORCOMMAND == "REQCHATROOMS"){						//Displays list of rooms
		rmChoice->clear();							// clears list to display refreshed list
		 

		std::vector<std::string>splitRm;
		boost::split(splitRm, S,[](char c){return c == ',';});
		int rmEle = splitRm.size();
		int maxRms = (rmEle-1)/5;	
		char room[30];
		char prvtValue[4];

		i = 0;
		k = 3;
		
		while(i < maxRms ){	
			j=0;
			std::string rmInfo;
			while(j<5){
				roomArray[i][j] = splitRm[k];
				if(j==3){
					if(roomArray[i][j] == "1"){
						strcpy(prvtValue,"YES"); 
					}
					else{
						strcpy(prvtValue,"NO ");
					}
				}
				else if (j==4){
					sprintf(room, "%.2i\t%-12s\t%s\t%s\n", stoi(roomArray[i][0]), roomArray[i][1].c_str(), prvtValue, roomArray[i][4].c_str());
					rmChoice->add(room);
				}
				k++;
				j++;
			}
			i++;
		}
	  }

	if(MAJORCOMMAND == "REQUSERS"){					//Displays list of users

		usrl->clear();

		char usrList[30];		
		std::vector<std::string> splitUsr;
		boost::split(splitUsr, S,[](char c){return c == ',';});
		int usrEle = splitUsr.size();
		maxUsr = (usrEle-2)/3;
		
		const char *name = usersChosenName.c_str();
		
		i = 0;
		k = 3;
		while(i<maxUsr){
			j=0;
			while(j<3){
				userArray[i][j] = splitUsr[k];
				if (j==2) {
					const char *uName = splitUsr[k-1].c_str();
					if(strcmp(name, uName) == 0) {
						usersCurrentRoom = splitUsr[k];
						printf("\nUSERS CURR ROOM: %s\n",usersCurrentRoom.c_str());
						sprintf(usrList, "@b%.3i\t@b%-12s\t@b%.2i\n", stoi(userArray[i][0]), userArray[i][1].c_str(), stoi(userArray[i][2]));
					}
					else{
						sprintf(usrList, "%.3i\t%-12s\t%.2i\n", stoi(userArray[i][0]), userArray[i][1].c_str(), stoi(userArray[i][2]));
					}
					usrl->add(usrList);
				}
				k++;
				j++;			
			}
			i++;
		}
	}

	if(MAJORCOMMAND == "INVITE"){					// Invite has been received 
		std::vector<std::string> splitInvite;
		boost::split(splitInvite, S,[](char c){return c == ',';});
		roomInvite = splitInvite[3];
		
		std::string inviteMsg = "You have been invited to room:" + roomInvite + ".\n Select ACCEPT/DECLINE \n\0";
		buff->append ( inviteMsg.c_str () );
		rmChangeAccept.show();
		rmChangeDecline.show();

	}
	
	if(MAJORCOMMAND == "REQUUID"){					// Jesus Invite has been received 
		std::vector<std::string> splitUUID;
		boost::split(splitUUID, S,[](char c){return c == ',';});
		myUUID = splitUUID[3];

	}

	//*********************** Notifies the user of the new room name of the room entered **************************/
	if(MAJORCOMMAND == "REQUESTCHATROOM"){
		
		int roomNum = stoi(splitMsg[3]);
		usersCurrentRoom = roomArray[roomNum-1][3];
		std::string roomName = roomArray[roomNum-1][1];		
		std::string roomMsg = "You have entered room: " + roomName + "\n\0";
		buff->append(roomMsg.c_str());

	}
	
	if(MAJORCOMMAND == "RESETROOM"){
		const char* kickRoom = splitMsg[3].c_str();
		const char* myRoom = usersCurrentRoom.c_str();		
		
		if (strcmp(myRoom,kickRoom) == 0){
			std::string kickMsg = "You have been kicked to The Lobby. Please select a new room.\n\0";
			printf("RESETROOM\n");
			usersCurrentRoom = "0";
			std::string changeRoom = usersChosenName + ",0";
			notify_Server(changeRoom, "CHANGECHATROOM");
			notify_Server(usersChosenName,"REQCHATROOMS");			//	Request Room Table update
			notify_Server(usersChosenName,"REQUSERS");				//	Request User Table update
			buff->append(kickMsg.c_str());
		}
	}

	if(MAJORCOMMAND == "KICKUSER"){
	//SERVER_MSG::0.<CRC>,1.<TIME>,2.<MAJORCOMMAND>,3.<KICKEDUSERSNAME>,4.<ROOMNUMBEROFKICKEDUSER>
		
				
		printf("\n\n/BEEN KICKED\n\n");

		
		std::string kickMsg = "You have been kicked to The Lobby. Please select a new room.\n\0";
		buff->append(kickMsg.c_str());
				
		notify_Server(usersChosenName,"REQCHATROOMS");			//	Request Room Table update
		notify_Server(usersChosenName,"REQUSERS");				//	Request User Table update
		
		usersCurrentRoom = "0";
				
	}
	

  }
  if (disp)
  {
	disp->show_insert_position();
	disp->scroll(9999999,0);
	disp->wrap_mode(20,0);
    disp->show ();

  }
  
	
	
	
	
	win.show ();

	win.wait_for_expose();
	Fl::flush(); 
	
	Fl::unlock(); 
	//mtx.unlock();
		
	T.clear();
	S.clear();
}

static void cb_clear ()
{

   if (buff)	// clear chat box
   {
     buff->remove (0,  buff->length () );
   }
   
}



std::string printPadding(int x){
	std::string pad = "";
	for(int i = 0; i <x; i++){
		pad = pad + " ";
	}
	return pad;
}

/**************************** UNIT TEST 1 FOR CLIENT TO SERVER CHECK STRING WITH MAJORCOMMAND "CHANGECHATROOM" **************************************/
static void clientUnitTest1(std::string testString){
	//SERVER_MSG::0.<CRC>,1.<TIME>,2.<MAJORCOMMAND>,3.<A_USERS_NAME>,4.<STRING>
	std::vector<std::string> splitSTR;
	boost::split(splitSTR, testString,[](char c){return c == ',';});
	int strSize = splitSTR.size();

	// Only two messages that are sent to the server will ever have more than 5 elements when split into a vector split by ','
	// Testing for correct message sent when user requests to change rooms, CHANGECHATROOM

	std::string str = "CHANGECHATROOM";

	if (strcmp(splitSTR[2].c_str(), str.c_str()) == 0){		// TEST FOR MAJORCOMMAND CHANGECHATROOM
		if(strSize != 5){				// TEST FOR NUMBER OF ELEMENTS IN MESSAGE SEPERATED BY ','
			testsFailed++;
			printf("\nMESSAGE SENT TO SERVER WAS TOO SHORT \nTEST FAIL\n");
			printf("TOTAL PASSED: %d\n", testsPassed);
			printf("TOTAL FAILED: %d\n", testsFailed);

		}
		else {
			if(strcmp(splitSTR[3].c_str(), usersChosenName.c_str()) != 0){	// TEST FOR MESSAGE CONTAINING CORRECT USERSCHOSENNAME
				testsFailed++;
				printf("\nMESSAGE SENT TO SERVER WAS SENT WITH WRONG USER NAME \nTEST FAIL\n");
				printf("TOTAL PASSED: %d\n", testsPassed);
				printf("TOTAL FAILED: %d\n", testsFailed);
			}
			else{
				int testRoom = stoi(splitSTR[4]);			
				if((testRoom > 0) && (testRoom <= 10)){		// TEST FOR ROOM NUMBER CHANGING TO IS BETWEEN 0 AND 10
					testsPassed++;
					printf("\nMESSAGE SENT TO SERVER IS GOOD \n TEST PASSED\n");
					printf("TOTAL PASSED: %d\n", testsPassed);
					printf("TOTAL FAILED: %d\n", testsFailed);
				}
				else{
					testsFailed++;
					printf("\nMESSAGE SENT TO SERVER HAS INVALID ROOM NUMBER \nTEST FAIL\n");
					printf("TOTAL PASSED: %d\n", testsPassed);
					printf("TOTAL FAILED: %d\n", testsFailed);
				}
			}
		}
	}
}


/************************************ MESSAGES THAT ARE SENT TO THE SERVER ************************************************************************/
static void notify_Server(std::string myText, std::string MAJORCOMMAND)	// use this function to inform server special action to be taken
{
	std::string tempCRC = "TempCRC";
	std::string notifyServerText = "," + timeStamp() + "," + MAJORCOMMAND + "," + myText + '\0';	// the message to send to server
	chat_message msg;

	std::string theCRC = msg.createCrc32(notifyServerText);
	notifyServerText = theCRC + notifyServerText;
	
	
	msg.body_length(notifyServerText.length() + 1);			// the size of the message
	std::memset (msg.body(), 0, msg.body_length());			// ensure it is null terminated
	std::memcpy (msg.body(), ( const char *) notifyServerText.c_str(), msg.body_length()-1); // memcpy(dest,src,size)
	msg.encode_header();
	std::cout << "\nNOTIFY SERVER: " << msg.body() << std::endl;
	c->write(msg);

	clientUnitTest1(notifyServerText);				// RUN UNIT TEST 1 FOR ALL MESSAGES OUTBOUND
}

static void cb_input1 (Fl_Input*, void * userdata)	 // FLTK User Text Window
{
	std::string userTemp(input1.value());
	input1.cut();

	std::string userText = usersChosenName + "," + userTemp;
	notify_Server(userText,"SENDTEXT");
	
}

static void cb_userLogin (Fl_Input*, void * userdata) 	// FLTK Callback for Username Login 
{
	std::string userName(userLogin.value(), 0, 10 );	// convert FLTK text value to string
	usersChosenName = userName; 						// set global user name so other functions can use it
	userLogin.cut();
	
	std::vector<std::string> splitNick;
	boost::split(splitNick, userName,[](char c){return c == ',';});
	int nameSize = splitNick.size();

	if (nameSize == 1){
	
		
		usersCurrentRoom = "0";								// User starts in room 0
		userHost = 0;										// User is not Host, 0 = False
		notify_Server(userName,"NICK");						// Send the users chosen name and the CN (change name) code
		displayLobby();										// User Provided username, so display lobby now
		notify_Server(usersChosenName,"REQCHATROOMS");			//	Request Room Table update
		notify_Server(usersChosenName,"REQUSERS");				//	Request User Table updater


	}
	else{
		std::string invalidNick = "INVALID NICK: Do not use a comma in your name\n\0";
		buff->append ( invalidNick.c_str () );
	}

		
}

static void cb_changeRoom (Fl_Input*, void * userdata) // FLTK Callback for changing rooms
{
	// Format should be 2 digit code: CR, UserName, newRoom ... EX: \CR,userx,2
	
	c->setLastCRC("nullCRC");
	lastCRC = c->getLastCRC();										// The last message recieved, reset to 0
	newRoom = rmChoice->value();
	std::string room = std::to_string(newRoom);					// Takes input int and changes to string

	cnfrmPass.hide();						// Hide if user leaving room after failing password to get into room
	if (roomArray[newRoom-1][3]=="1"){
		cnfrmPass.show();
	}
	
	else {
		if(newRoom != 0){
			buff->remove (0, buff->length () );
			std::string changeRoom(usersChosenName + ',' + room);	// Creates room change message for server

			if (userHost == 1){										// If user is Host
				userHost = 0;										// Remove Host status
				notify_Server(usersCurrentRoom, "RESETROOM");				// Notify server to reset room
				kickUser.hide();			
			}

			notify_Server(changeRoom, "CHANGECHATROOM");			// Change rooms
			notify_Server(usersChosenName,"REQCHATROOMS");			//	Request Room Table update
			notify_Server(usersChosenName,"REQUSERS");				//	Request User Table update
			usersCurrentRoom = room;								// value for current room changed
			notify_Server(usersChosenName, "REQCHATROOM");			// Request new chatroom name
			createRm.show();		
			inviteUser.show();
		}
	}
}

static void cb_cnfrmPass(Fl_Input*, void * userdata)
{
	std::string room = std::to_string(newRoom);
	std::string typedPassword(cnfrmPass.value(), 0, 10 );
	cnfrmPass.cut();

	const char *entrdPass = typedPassword.c_str();
	const char *strdPass = roomArray[newRoom-1][2].c_str();

	if (strcmp(entrdPass, strdPass) == 0){
		std::string changeRoom(usersChosenName + ',' + room);	// Creates room change message for server

		if (userHost == 1){										// If user is Host
			userHost = 0;										// Remove Host status
			notify_Server(usersCurrentRoom, "RESETROOM");				// Notify server to reset room
			kickUser.hide();
		}
		
		buff->remove (0, buff->length () );						// Clear previous messages in display
		notify_Server(changeRoom, "CHANGECHATROOM");			// Change rooms
		notify_Server(usersChosenName,"REQCHATROOMS");			//Request Room Table update
		notify_Server(usersChosenName,"REQUSERS");				//Request User Table update
		usersCurrentRoom = room;								// value for current room changed
		//notify_Server(usersChosenName, "REQCHATROOM");		// Request new chatroom name
		cnfrmPass.hide();
		createRm.show();
		inviteUser.show();
		
	}

	else {
		std::string passFail = "INCORRECT PASSWORD.\n\0";	// Message for failed password attempt
		buff->append ( passFail.c_str () );			
	} 

}

static void cb_inviteUser (Fl_Input*, void * userdata)
{
	// Format should be 2 digit code: IN, invited name, room inviting to
	// Need to implement check for ',' in name given. Maybe split name by ',' and take first part if ',' present
	std::string inviteName(inviteUser.value(), 0, 10 );
	inviteUser.cut();						//clears text box after receiving name
	std::string invMsg;
	const char *cmpName;
	const char *name = inviteName.c_str();

	int same;
	int i = 0;
	while(i < 10){
		cmpName = userArray[i][1].c_str();
		same = strcmp(name,cmpName);
		if(same == 0){
			invMsg = inviteName + " was sent an invite.\n\0";	
			std::string invSent(inviteName + ',' + usersCurrentRoom);
			notify_Server(invSent, "INVITE");
			break;
		}
		else if (i==9){
			invMsg = inviteName + " is not a valid username.\n\0";
		}
		i++;		
	} 
	buff->append ( invMsg.c_str () );

}

//*****************************************************  Kick user if the client is the host **************************///
static void cb_kickUser( Fl_Input*, void * userdata)
{
	// FIND HOSTS CURRENT ROOM
	int i = 0;
	while( i < 10){
		if(userArray[i][1].c_str() == usersChosenName){
			usersCurrentRoom = userArray[i][2];
			break;
		}
	}
	
	// Need to implement check for ',' in name given. Maybe split name by ',' and take first part if ',' present
	std::string kickName(kickUser.value(), 0, 10 );
	kickUser.cut();							// clears text box after receiving name
	std::string kickMsg;
	const char *cmpName;
	const char *name = kickName.c_str();
	const char *myRoom = usersCurrentRoom.c_str();
	const char *theirRoom;
	
	printf("\nKICKUSER\n");
	int same;
	i = 0;
	
	if (strcmp(name,usersChosenName.c_str()) != 0){
		while(i < 10){
			cmpName = userArray[i][1].c_str();
			same = strcmp(name,cmpName);
		
			printf("\ncmp: %s , nam: %s\n",cmpName,name);
			if(same == 0){
			printf("SAME");
				theirRoom = userArray[i][2].c_str();		//changed [i][0] to [i][2]  for correct value
			
				printf("/myR: %s , thR: %s\n",myRoom,theirRoom);
				if(strcmp(myRoom,theirRoom) == 0){
					kickMsg = kickName + " has been kicked from the room.\n\0";
					std::string kickSent(kickName + ',' + usersCurrentRoom);

					notify_Server(kickSent, "KICKUSER");
				}
				else {
					kickMsg = "The user you entered is not in your room.\n\0";
				}
				break;
			}
			else if (i==9){
				kickMsg = kickName + " is not a valid username.\n\0";
			
			}

			i++;		
		}
	}
	else{
		kickMsg = "You cannot kick yourself out. \n\0";
		
	}

	notify_Server(usersChosenName,"REQUSERS");		//Request User Table update
	buff->append (kickMsg.c_str () );

}

static void reqUUID_CB(void *data) {             			 		// timer callback
		
		
		notify_Server(usersChosenName,"REQUUID");					//Request User Table update
		//notify_Server(usersChosenName,"REQCHATROOMS");			//Request Room Table update
		//notify_Server(usersChosenName,"REQUSERS");				//Request User Table update
        //Fl::repeat_timeout(5, Timer_CB, data);					// 5 sec timer

}


static void Timer_CB(void *data) {             			 		// timer callback
		
		notify_Server(usersChosenName,"REQCHATROOMS");			//Request Room Table update
		notify_Server(usersChosenName,"REQUSERS");				//Request User Table update
        Fl::repeat_timeout(5, Timer_CB, data);					// 5 sec timer

}

static void msgTimer_CB(void *data) {             		 				// timer callback
		
		lastCRC = c->getLastCRC();										// get the last CRC code
		std::string getMessages = usersChosenName + "," + lastCRC;
		notify_Server(getMessages,"REQTEXT");							// Request New Messages
        Fl::repeat_timeout(2, msgTimer_CB, data);						// .5 sec timer           --------- CHANGE LATER

}


static void cb_displayCreateRoom()
{	//Shows text boxes for information to be filled and confirm button
	// may change prvtYN text box to a bubble option for yes or no
	// may change visibility of password text box to selection of prvtYN choice
	// need to include a cancel button to hide items again
	
	createRm.hide();
	clearBut.hide();
	rmName.show();
	privateCheck.show();
	cnfrmRm.show();
	cancelRm.show();
}

static void cb_privateCheck()
{
	if (rmPassword.visible() == false)	{
		rmPassword.show();
	}
	else{
		rmPassword.cut();
		rmPassword.hide();
	}

}

static void cb_confirmRoom()
{	// Create new room with default settings made if left blank
	std::string newRmName(rmName.value(), 0, 10);
	std::string password(rmPassword.value(), 0, 10);
	std::string prvt;
	std::string fixRoom;
	int inputDefect = 0;
	

	std::vector<std::string>splitName;
	boost::split(splitName, newRmName,[](char c){return c == ',';});

	
	std::vector<std::string>splitPassword;
	boost::split(splitPassword, password,[](char c){return c == ',';});

	
	if ((newRmName == "") || (splitName.size() > 1)){
		inputDefect = 1;
		fixRoom = "Invalid input for Room Name.\n\0";
		buff->append(fixRoom.c_str());
	}
	if (rmPassword.visible() == true){
		prvt = '1';		
		if (password == "") {
			inputDefect = 1;
			fixRoom = "Invalid input for Password.\n\0";
			buff->append(fixRoom.c_str());
		}
		else if (splitPassword.size() > 1){
			inputDefect = 1;
			fixRoom = "Invalid input for Password.\n\0";
			buff->append(fixRoom.c_str());
		}
	}
	else {
		prvt = '0';
		password = "NONE";
	}
	
	if (inputDefect == 1){
		fixRoom = "Errors must be corrected to create a room.\n\0";
		buff->append(fixRoom.c_str());
	}
	else {
		// Msg to be sent to server to create a room
		std::string ctMsg(newRmName + ',' + usersChosenName + ',' + password + ',' + prvt);
		buff->remove (0,  buff->length () );				// Clear previous messages in display


		if (userHost == 1){										// If user is Host
			userHost = 0;										// Remove Host status
			notify_Server(usersCurrentRoom, "RESETROOM");				// Notify server to reset room
			kickUser.hide();			
		}

		notify_Server(ctMsg, "NAMECHATROOM");

		userHost = 1;
	
		kickUser.align(FL_ALIGN_TOP);				//Label on Top
		kickUser.show();

		rmName.cut();
		rmPassword.cut();
		rmName.hide();
		rmPassword.hide();
		privateCheck.clear();
		privateCheck.hide();
		cnfrmRm.hide();
		cancelRm.hide();
		createRm.show();
		clearBut.show();

		notify_Server(usersChosenName,"REQCHATROOMS");		//Request Room Table update
		notify_Server(usersChosenName,"REQUSERS");		//Request User Table update
		notify_Server(usersChosenName,"REQCHATROOM");
	}
}

static void cb_cancelRoom()
{	// Cancel create new room with default settings made blank
	createRm.show();
	rmName.hide();
	rmPassword.hide();
	privateCheck.clear();
	privateCheck.hide();
	cnfrmRm.hide();
	cancelRm.hide();
	createRm.show();
	clearBut.show();
}

static void cb_acceptInvite()
{
	cnfrmPass.hide();
	std::string changeRoom(usersChosenName + ',' + roomInvite);	// Creates room change screen for server

	if (userHost == 1){								// If user is Host
		userHost = 0;								// Remove Host status
		notify_Server(usersCurrentRoom, "RR");		// Notify server to reset room
		
		kickUser.hide();
	}
	
	buff->remove (0,  buff->length () );			// Clear previous messages in display
	notify_Server(changeRoom, "CHANGECHATROOM");	// Change rooms
	notify_Server(usersChosenName,"REQCHATROOMS");	// Request Room Table update
	notify_Server(usersChosenName,"REQUSERS");		// Request User Table update
	usersCurrentRoom = roomInvite;					// value for current room changed
	notify_Server(usersChosenName, "REQCHATROOM");		// Request new chatroom name

	rmChangeAccept.hide();
	rmChangeDecline.hide();

}


static void cb_declineInvite()
{

	rmChangeAccept.hide();
	rmChangeDecline.hide();

}


static void cb_test(){
	
	boost::asio::io_service io_service;
    tcp::resolver resolver(io_service);
	
	auto endpoint_iterator = resolver.resolve({ "127.0.0.1", "8000" });
	
	testc = new chat_client (io_service, endpoint_iterator, &cb_recv);
    testt = new std::thread ([&io_service](){ io_service.run(); });
	
	notify_Server("TestBot","NICK");											//CREATE USER
	notify_Server(usersChosenName,"REQCHATROOMS");								//	Request Room Table update
	notify_Server(usersChosenName,"REQUSERS");									//	Request User Table update
	Fl::wait(3);
	
	std::string ctMsg("TestRoom,TestBot,none,0");	// CREATE ROOM
	notify_Server(ctMsg, "NAMECHATROOM");
	notify_Server(usersChosenName,"REQCHATROOMS");								//	Request Room Table update
	notify_Server(usersChosenName,"REQUSERS");									//	Request User Table update
	Fl::wait(3);
	
	std::string invSent(usersChosenName + ',' + "1");				// INVITE ME TO THE ROOM
	notify_Server(invSent, "INVITE");
	
}


void displayLobby(){
	
	testMe.show();
	userLogin.hide();
	input1.show();
	clearBut.show();
}

int displayAll(){
	
	// ADD ALL WIDGETS HERE ====================================================================
	// =========================================================================================
	
	win.begin ();

	/*
		o->box(FL_GTK_DOWN_BOX);
        o->color(FL_DARK1);
        o->labelsize(12);
        o->align(Fl_Align(FL_ALIGN_TOP));
		
		win.add(o);
	*/
		
		
	disp->buffer(buff);				// TEXT BOX	
	std::string msg = "WELCOME TO UBERCHAT\n\0";
	buff->append(msg.c_str());
	msg = "You are in The Lobby.\n\0";
	buff->append(msg.c_str());

	usrl->align(FL_ALIGN_TOP);			// USER LIST
	static int usrWidths[] = {40, 95, 27};
	usrl->column_widths(usrWidths);
	usrl->column_char('\t');

	rmChoice->align(FL_ALIGN_TOP);			// ROOM LIST
	rmChoice->callback ((Fl_Callback*) cb_changeRoom);
	static int rmWidths[] = {27, 80, 34, 100};
	rmChoice->column_widths(rmWidths);
	rmChoice->column_char('\t');
	
   	win.add (input1);			// INPUT ===============================
	input1.align(FL_ALIGN_TOP);				//Label on Top
	input1.hide();
	input1.callback ((Fl_Callback*)cb_input1,( void *) "Enter next:");
    	input1.when ( FL_WHEN_ENTER_KEY );
	
	win.add (userLogin);		// User Login ==========================
	userLogin.callback ((Fl_Callback*)cb_userLogin,( void *) "Enter next:");
    	userLogin.when ( FL_WHEN_ENTER_KEY );
		
	win.add(clearBut);			// CLEAR ===============================
	clearBut.hide();
    	clearBut.callback (( Fl_Callback*) cb_clear );


	win.add(createRm);			// CREATE ROOM =========================
	createRm.hide();
	createRm.callback (( Fl_Callback*) cb_displayCreateRoom );	
	rmName.hide();
	rmPassword.hide();

	win.add(privateCheck);
	privateCheck.align(FL_ALIGN_LEFT);
	privateCheck.hide();
	privateCheck.callback (( Fl_Callback*) cb_privateCheck );

	win.add(cnfrmRm);
	cnfrmRm.hide();
	cnfrmRm.callback (( Fl_Callback*) cb_confirmRoom );

	win.add(cancelRm);
	cancelRm.hide();
	cancelRm.callback (( Fl_Callback*) cb_cancelRoom );
	
	
	win.add(inviteUser);
	inviteUser.align(FL_ALIGN_TOP);
	inviteUser.hide();
	inviteUser.callback ((Fl_Callback*)cb_inviteUser,( void *) "Enter next:");
	inviteUser.when ( FL_WHEN_ENTER_KEY );

	win.add(kickUser);
	kickUser.callback ((Fl_Callback*)cb_kickUser,( void *) "Enter next:");
	kickUser.when ( FL_WHEN_ENTER_KEY );
	kickUser.hide();			//hide until user is host

	win.add(cnfrmPass);
	cnfrmPass.hide();
	cnfrmPass.callback ((Fl_Callback*)cb_cnfrmPass,( void *) "Enter next:");

	win.add(rmChangeAccept);
	rmChangeAccept.hide();
	rmChangeAccept.callback ((Fl_Callback*)cb_acceptInvite );

	win.add(rmChangeDecline);
	rmChangeDecline.hide();
	rmChangeDecline.callback ((Fl_Callback*)cb_declineInvite );
	
	win.add(testMe);			// TEST ==============================
	testMe.hide();
	testMe.callback (( Fl_Callback*) cb_test );	
	rmName.hide();
	rmPassword.hide();
	
	Fl::add_timeout(0, reqUUID_CB);            	  // setup a timer
	Fl::add_timeout(.5, Timer_CB);            	  // setup a timer
	Fl::add_timeout(.5, msgTimer_CB);             // setup a timer
	
	win.end ();
	win.show ();

	return Fl::run ();
  
}

int main ( int argc, char **argv) 
{

  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: chat_client <host> <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;
    tcp::resolver resolver(io_service);

    auto endpoint_iterator = resolver.resolve({ argv[1], argv[2] });
    c = new chat_client (io_service, endpoint_iterator, &cb_recv);
    t = new std::thread ([&io_service](){ io_service.run(); });

    // goes here, never to return.....
    testsPassed = 0;
    testsFailed = 0;
    displayAll();
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }
  // never gets here
  return 0;
}

std::string timeStamp(){

	ptime t = second_clock::local_time();
	std::string currentTime = to_iso_string(t);
	currentTime[15] = '.';				//Remove "," and change to "." to avoid error at server
	return currentTime;
	
}
