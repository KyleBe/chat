//
// chat_message.hpp 
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef CHAT_MESSAGE_HPP
#define CHAT_MESSAGE_HPP

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <boost/integer.hpp>
#include <boost/crc.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string.hpp>

class chat_message
{
public:
  enum { header_length = 4 };
  enum { max_body_length = 512 };

  chat_message()
    : body_length_(0)
  {
  }

  const char* data() const
  {
    return data_;
  }

  char* data()
  {
    return data_;
  }

  std::size_t length() const
  {
    return header_length + body_length_;
  }

  const char* body() const
  {
    return data_ + header_length;
  }

  char* body()
  {
    return data_ + header_length;
  }

  std::size_t body_length() const
  {
    return body_length_;
  }

  void body_length(std::size_t new_length)
  {
    body_length_ = new_length;
    if (body_length_ > max_body_length)
      body_length_ = max_body_length;
  }

  bool decode_header()
  {
    char header[header_length + 1] = "";
    std::strncat(header, data_, header_length);
    body_length_ = std::atoi(header);
    if (body_length_ > max_body_length)
    {
      body_length_ = 0;
      return false;
    }
    return true;
  }

	void encode_header()
	{
		char header[header_length + 1] = "";
		std::sprintf(header, "%4ld", body_length_);
		std::memcpy(data_, header, header_length);
	}
  
	std::string createCrc32(std::string crcMe)  {	// Create the CRC32 checksum after encoding the msg header, append to beginning of message
	
        boost::crc_32_type result;											//CRC32 storage variable
        result.process_bytes(&crcMe[0], crcMe.length()-1);		//Generate CRC32 based off of header/message
        int checksum = result.checksum();										//int to store CRC32 10 digit value
		
		std::stringstream ss;
		ss << std::hex << checksum; 			// int checksum
		std::string checksumStr (ss.str());		// get string version
		
		return checksumStr;								//return the hexed crc32
		
    }
	
	std::string getCrc32FromMessage (){	// Read the memory of the message and retrieve CRC32 checksum from it
		
		
		std::string temp = data_;
		std::string theMessage = temp.substr(5, body_length_ );				// remove the header from message
		
		std::vector<std::string> messageVec;								// vector of the separted strings from the message
		boost::split(messageVec, theMessage, [](char c){return c == ',';});
		
		printf("\ngetCrc32FromMessage: 0: %s ==== 1: %s ==== 2: %s\n", messageVec[0].c_str(),messageVec[1].c_str(),messageVec[2].c_str());
        return messageVec[0];
    }
	
	

private:
  char data_[header_length + max_body_length];
  std::size_t body_length_;
};

#endif // CHAT_MESSAGE_HPP